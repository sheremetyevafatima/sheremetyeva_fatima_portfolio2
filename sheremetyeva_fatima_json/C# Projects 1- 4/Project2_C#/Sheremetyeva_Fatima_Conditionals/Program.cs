﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sheremetyeva_Fatima_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {

            // Fatima Sheremetyeva
            // SDI Section #01
            // Conditionals
            // 01/15/17


           

                        //PROBLEM #1 TEMPERATURE CONVERTER

                        // creating boolean to break out of the loop when done calculating.
                        bool keepgoing = true;
                        bool corf = false;
                        // Using a do while loop to run the converter at least once
                        do
                        {

                            //Asking the user to enter temperature and number and storing it in a sting
                            Console.WriteLine("Please enter a temperature number:");

                            string tempNumString = Console.ReadLine();

                            // Declaring a variable for converted degrees
                            double convertedDegrees;

                            // Declaring a variable that will parse and store temperature number
                            double tempNum;

                            // If the user entered a number proceed
                            if (!(string.IsNullOrWhiteSpace(tempNumString)))
                            {
                                // Parsing temperature number 
                                tempNum = double.Parse(tempNumString);

                                // Asking the user if the number is in Celcius or Fahrenheit and storing the input
                                do { 
                                Console.WriteLine("Is it in degrees Celcius or Fahrenheit? Please Enter 'C' for Celsius or 'F' for Farenheit");

                                string degrees = Console.ReadLine();

                                    // If the user entered degree type proceed
                                    if (!(string.IsNullOrWhiteSpace(tempNumString)))
                                    {

                                        // if it is in Celcius perform (tempNum * 1.8 + 32);
                                        if (degrees == "C" || degrees == "c")
                                        {

                                            convertedDegrees = tempNum * 1.8 + 32;
                                            Console.WriteLine(tempNumString + " degrees Celcius converted to Fahrenheit is: " + convertedDegrees);
                                            keepgoing = false;
                                            corf = true;
                                        }
                                        else if (degrees == "F" || degrees == "f")
                                        {

                                            //if it is in Fahrenheit perform (tempNum - 32) * 0.5556;
                                            convertedDegrees = (tempNum - 32) * 0.5556;
                                            Console.WriteLine(tempNumString + " degrees Fahrenheit converted to Celsius is: " + convertedDegrees);
                                            keepgoing = false;
                                            corf = true;
                                        }
                                        else
                                        {
                                            Console.WriteLine("You entered something else. Please Enter 'C' for Celsius or 'F' for Farenheit");


                                        }
                                    }
                                    else
                                    {
                                        // If the user didn't enter anyting prompt him again
                                        Console.WriteLine("Please don't leave it blank. Is it in degrees Celcius or Fahrenheit?");

                                        degrees = Console.ReadLine();
                                    }

                                } while (!corf);
                            }
                            else
                            {
                                Console.WriteLine("Please don't leave it blank.");
                            }
                        }
                        while (keepgoing);



                        /*
                         * 45F is 7.2 C
                         * 37 c is 98.6 F
                         * 
                         * */
          

                        // PROBLEM #2 LAST CHANCE FOR GAS


                        // creating boolean to break out of the loop when done calculating.
                        bool stillooping = true;
                        bool promtagain = false;
                        bool promtinmore = false;
                        double howFullNum;
                        double milesPerGal ;
                        // Using a do while loop to run the converter at least once
                        do
                        {

                            //Asking the user to enter how many gallons does his car hold
                            Console.WriteLine("How many gallons does your car tank hold?");

                            string galNumString = Console.ReadLine();

                            // Declaring a variable for how many gallons left
                            double gallonsLeft;

                            // Declaring a variable that will parse and store how many gallons car holds
                            double galNum;

                            // If the user entered a number proceed
                            if (!(string.IsNullOrWhiteSpace(galNumString)))
                            {
                                // Parsing  
                                galNum = double.Parse(galNumString);


                                do
                                {
                                    Console.WriteLine("How	full	is	your	gas	tank?	(in	%)");

                                    string howFullSrtring = Console.ReadLine();

                                    if (!(string.IsNullOrWhiteSpace(galNumString)))
                                    {
                                        howFullNum = double.Parse(howFullSrtring);
                                        promtagain = true;

                                        do {
                                            Console.WriteLine("How	many	miles	per	gallon	does	your	car	go?");
                                            string milesPerG = Console.ReadLine();
                                            if (!(string.IsNullOrWhiteSpace(galNumString)))
                                            {
                                                milesPerGal = double.Parse(milesPerG);
                                                promtinmore = true;

                                                double calcWhatInTank = galNum * (howFullNum / 100);
                                                double milesCanGo = calcWhatInTank * milesPerGal;


                                                if (milesCanGo >= 200)
                                                {
                                                    Console.WriteLine("Yes, you can drive {0} more miles and you can make it without stopping for gas!", milesCanGo);
                                                }

                                                else
                                                {
                                                    Console.WriteLine("You only have {0} gallons of gas in you tank, better get gas now while you can!", milesCanGo);
                                                }
                                    promtagain = true;
                                    stillooping = false;
                                    break;


                                            }
                                            else 
                                            {
                                                // If the user didn't enter anyting prompt him again
                                                Console.WriteLine("Please don't leave it blank.");

                                                howFullSrtring = Console.ReadLine();
                                            }


                                        } while (!promtinmore);
                                    }
                                    else
                                    {
                                        // If the user didn't enter anyting prompt him again
                                        Console.WriteLine("Please don't leave it blank.");

                                        howFullSrtring = Console.ReadLine();
                                    }

                                } while (!promtagain);
                            }
                            else
                            {
                                Console.WriteLine("Please don't leave it blank.");
                            }
                        }
                        while (stillooping);


                        // Gallons	-20	, Gas	Tank	=	50%	full,	MPG- 25
                        //     Result – “Yes,	you	can	drive	250 more	miles	and	you	can	make	it	without	
                          //      stopping	for	gas!”
                        //     Gallons	-12	, Gas	Tank	=	60%	full,	MPG- 20
                        //        § Result – “You only	have 144 gallons	of	gas	in	your	tank,	better	get	gas	
                         //       now	while	you	can!”
                        // * Gallons	-18	, Gas	Tank	=	62%	full,	MPG- 26
                       //  *  Result – “Yes,	you	can	drive	290.16 more	miles	and	you	can	make	it	without	
                       //         stopping	for	gas!”
                       


            //PROBLEM #3 GRADE LETTER Calculator
            // Declaring a boolean to stop the loop when needed



                bool stillgo = false;
                        // Prompting the user to enter his grade and storing it in a sting
                        Console.WriteLine("Please enter  your course grade in %");

                        string courseGradeString = Console.ReadLine();

                        do
                        {
                            // if the usesr entered a value, convert it to in
                            if (!(string.IsNullOrWhiteSpace(courseGradeString)))
                            {
                                int courseGradeNum = int.Parse(courseGradeString);
                                stillgo = true;
                                // Conditionlas to check which grade letter the user got
                                if (courseGradeNum >= 90 && courseGradeNum <= 100) {

                                    Console.WriteLine(" You have a {0}, which means you have earned a(n) A in the class!" , courseGradeNum);
                                } else if (courseGradeNum >= 80 && courseGradeNum <= 89)
                                {
                                    Console.WriteLine(" You have a {0}, which means you have earned a(n) B in the class!", courseGradeNum);

                                }
                                else if (courseGradeNum >= 73 && courseGradeNum <= 79)
                                {
                                    Console.WriteLine(" You have a {0}, which means you have earned a(n) C in the class!", courseGradeNum);

                                }
                                else if (courseGradeNum >= 70 && courseGradeNum <= 72)
                                {
                                    Console.WriteLine(" You have a {0}, which means you have earned a(n) D in the class!", courseGradeNum);

                                }
                                else if (courseGradeNum >= 0 && courseGradeNum <= 69)
                                {
                                    Console.WriteLine(" You have a {0}, which means you have earned a(n) F in the class!", courseGradeNum);

                                }

                            }
                            else
                            {
                                // if the user didn't enter anything, prompt again
                                Console.WriteLine("Please don't leave it blank.");

                                courseGradeString = Console.ReadLine();
                            }
                        } while (!stillgo);
                        

            /*Grade	– 92%	
§ Result - “You	have	a 92%,	which	means	you	have	earned	a(n) A in	the	
class!”
o Grade	– 80%	
§ Result - “You	have	a 80%,	which	means	you	have	earned	a(n) B in	the	
class!”
o Grade	– 67%	
§ Result - “You	have	a 67%,	which	means	you	have	earned	a(n) F in	the	
class!”
             * 
             */

            //PROBLEM #4 DISCOUNT DOUBLE CHECK

            // Declaring all variables

            bool loopit = false;
            double firstNum = 0;
            double secondNum =0;
            double total =0;
            do
            {
                // Asking the user for input and storning and parsing it if it's no blank
                Console.WriteLine("Cost of First Item in $:");

                string firstString = Console.ReadLine();
                if (!(string.IsNullOrWhiteSpace(firstString)))
                {
                  firstNum = double.Parse(firstString);

                    loopit = true;
                }
                else
                {
                    // prompt again if it's black
                    Console.WriteLine("Please don't leave it blank");
                
                }


            } while (!loopit);



                       bool loopit1 = false;

            do
            {
                Console.WriteLine("Cost of Second Item in $:");

                string secondString = Console.ReadLine();
                if (!(string.IsNullOrWhiteSpace(secondString)))
                {
                     secondNum = double.Parse(secondString);

                    loopit1 = true;

                }
                else
                {
                    Console.WriteLine("Please don't leave it blank");
                
                }


            } while (!loopit1);
            //add two number togethe and store the sum
             total = firstNum + secondNum;

            // Calculating discounts when applicable
            if (total > 100)
            {

                total -= (total * 0.1);
                Console.WriteLine("Your total purchase is {0}, which includes your 10% discount", total);
            }
            else if (total >= 50 && total <= 100)
            {

                total -= (total * 0.05);
                Console.WriteLine("Your total purchase is {0}, which includes your 5% discount", total);
            }
            else {

                Console.WriteLine("Your total purchase is {0}", total);
            }


            /* First	Item	Cost	- $	45.50,	Second	Item	Cost	- $75.00,	Total	- $108.45
§ Results - “Your	total	purchase	is	$108.45,	which	includes	your	10%	
discount.”
o First	Item	Cost	- $	30.00,	Second Item	Cost	- $25.00,	Total	- $52.25
§ Results - “Your	total	purchase	is	$52.25,	which	includes	your	5%	discount.”
o First	Item	Cost	- $	5.75,	Second	Item	Cost	- $12.50,
             * 
             * 
             */
        }
    }
}
