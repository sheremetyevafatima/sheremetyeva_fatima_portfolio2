﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sheremetyeva_Fatima_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {

            //Fatima Sheremetyeva
            // SDI Section #01
            // Logic Loops
            // 01/15/17

            //PROBLEM #1 LOGICAL OPERATORS - TIRE PRESSURE

            // Declare variables

          
                        bool keeplooping = true;
                        string firstFront = "";
                        double firstFrontP;
                        double[] pressArray = new double[4];
                        do
                        {
                            // Get user's input and store it
                            Console.WriteLine("Please enter first front tire pressure:");
                            firstFront = Console.ReadLine();
                            // if it parsed proceed to storing it an array
                            if ((double.TryParse(firstFront, out firstFrontP)))
                            {

                                pressArray[0] = firstFrontP;

                                keeplooping = false;
                            }
                        } while (keeplooping);

                        string secondFront = "";
                        double secondFrontP = 0;
                        keeplooping = true;
                        do
                        {
                            // Get user's input and store it
                            Console.WriteLine("Please enter second front tire pressure:");
                            secondFront = Console.ReadLine();
                            // if it parsed proceed to storing it an array
                            if ((double.TryParse(secondFront, out secondFrontP)))
                            {

                                pressArray[1] = secondFrontP;

                                keeplooping = false;
                            }
                        } while (keeplooping);

                        string firstRare = "";
                        double firstRareP = 0;
                        keeplooping = true;
                        do
                        {
                            // Get user's input and store it
                            Console.WriteLine("Please enter first rare tire pressure:");
                            firstRare = Console.ReadLine();
                            // if it parsed proceed to storing it an array
                            if ((double.TryParse(firstRare, out firstRareP)))
                            {

                                pressArray[2] = firstRareP;

                                keeplooping = false;
                            }
                        } while (keeplooping);



                        string secondRare = "";
                        double secondRareP = 0;
                        keeplooping = true;
                        do
                        {
                            // Get user's input and store it
                            Console.WriteLine("Please enter first rare tire pressure:");
                            secondRare = Console.ReadLine();
                            // if it parsed proceed to storing it an array
                            if ((double.TryParse(secondRare, out secondRareP)))
                            {

                                pressArray[3] = secondRareP;

                                keeplooping = false;
                            }
                        } while (keeplooping);


                        if ((pressArray[0] == pressArray[1]) && (pressArray[2] == pressArray[3]))
                        {

                            Console.WriteLine("The tires pass spec!");

                        }
                        else {
                            Console.WriteLine("Get your tires checked out!");

                        }



                        /*Front	Left	– 32,	Front	Right	– 32,	Back	Left	-30	Back	Right- 30		- Tires	OK
                          Front	Left	– 36,	Front	Right	– 32,	Back	Left	-25	Back	Right- 25		- Check	Tires
                         * 
                         */





            //PROBLEM #2 LOGICAL OPERATORS : MOVIE TICKET PRICE

            //Declare variables
            bool stillgoing = true;
            string ageString = "";
            string timeString = "";
            int age = 0;
            int time = 0;

            do {
                //Prompt the user and store it
                Console.WriteLine("Please enter your age:");

                ageString = Console.ReadLine();

                //parse it if it converts to integer

                if ((int.TryParse(ageString, out age))) {

                    stillgoing = false;
                }

            } while (stillgoing);


            stillgoing = true;
            do
            {


                //Prompt the user and store it
                Console.WriteLine("Please enter the time:");

                timeString = Console.ReadLine();

                //parse it if it converts to integer
                if ((int.TryParse(timeString, out time)))
                {
                    stillgoing = false;
                }

            } while (stillgoing);

            if ((age >= 55 || age <= 10) || (time >= 14 && time <= 17))
            {

                Console.WriteLine("The ticket price is $7");
            }
            else {
                Console.WriteLine("The ticket price is $12 ");

            }

            /*
             * Age	– 57,	Time	– 20,	Ticket	Price	- $7.00
o Age	– 9,	Time	– 20,	Ticket	Price	- $7.00
o Age	– 38,	Time	– 20,	Ticket	Price	- $12.00
             */

        }
    }
}
