﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sheremetyeva_Fatima_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {


            //Fatima Sheremetyeva
            //SDI Section #01
            //ArrayLists
            //01/21/17
        

            //call the function
            someArrayList();
        }
        //create a function to combine elements of arraylists in a sentence
        public static void someArrayList() {

            //create the fist ArrayList
            ArrayList firstOne = new ArrayList() {"Pasta", "Sushi", "Perogie", "Schnitzel", "Tacos"};
            //Create the second ArrayList
            ArrayList secondOne = new ArrayList() { "Italian", "Japanese", "Polish", "Austrian", "Mexican" };
            //create a for loop to cycle through both ArrayLists at the same time and print out a combined sentence
            for (int i=0; i < firstOne.Count ; i++) {

                Console.WriteLine("{0} is {1} dish \n", firstOne[i], secondOne[i]);

            }

            Console.WriteLine("New ArrayList:\n");
            //remove an element from the ArrayLlist
            firstOne.Remove("Pasta");
            //remove an element from the ArrayLlist
            firstOne.Remove("Sushi");
            //remove an element from the ArrayLlist
            secondOne.Remove("Italian");
            //remove an element from the ArrayLlist
            secondOne.Remove("Japanese");
            //add and element to the ArrayList
            firstOne.Insert(0, "Burger");
            //add and element to the ArrayList
            secondOne.Insert(0, "American");


            //create a for loop to cycle through both ArrayLists at the same time and print out a combined sentence
            for (int i = 0; i < firstOne.Count; i++)
            {

                Console.WriteLine("{0} is {1} dish \n", firstOne[i], secondOne[i]);

            }
        }
    }
}
