﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sheremetyeva_Fatima_CountFish
{
    class Program
    {
        static void Main(string[] args)
        {


            //Fatima Sheremetyeva
            //SDI Section #01
            //Fish Count
            // 01/15/2017
            
            // Create an array with colors
            string[] fishes = new string[] { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" };
            //Associate colors with numbers
            string[] colors = new string[] { "", "red", "blue", "green", "yellow" };
            int pick = 0;
            do
            {
                //Ask for user's unput and parse it
                Console.WriteLine("Choose	(1) for	Red, (2) for Blue, (3) for Green, and (4) for Yellow");
                pick = int.Parse (Console.ReadLine());

                // input validation
            } while (!(pick > 0 && pick < 5));

            int fish_this_color = 0;
            string this_color = colors[pick];
            //cycle through the array and count the colors
            for (int i = 0; i < fishes.Length; i++)
            {
                if (fishes[i] == this_color)
                {
                    fish_this_color++;
                }
            }
            // output results to the user

            Console.WriteLine("In the fish tank there are {0} fish of the color {1}.", fish_this_color, this_color);


        }
    }
}
