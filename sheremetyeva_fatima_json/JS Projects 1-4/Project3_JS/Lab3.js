// Create an array with colors
           var fishes =["red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" ];
            //Associate colors with numbers
           var colors = [ "", "red", "blue", "green", "yellow" ];
            var pick = 0;
            do
            {
                //Ask for user's unput and parse it
               pick =  prompt("Choose	(1) for	Red, (2) for Blue, (3) for Green, and (4) for Yellow");
               
               pick = parseInt(pick, 10);

                // input validation
            } while (!(pick > 0 && pick < 5));

           var fish_this_color = 0;
            var this_color = colors[pick];
            //cycle through the array and count the colors
            for (var i = 0; i < fishes.length; i++)
            {
                if (fishes[i] == this_color)
                {
                    fish_this_color++;
                }
            }
            // output results to the user

            console.log("In the fish tank there are " + fish_this_color + " fish of the color " + this_color);