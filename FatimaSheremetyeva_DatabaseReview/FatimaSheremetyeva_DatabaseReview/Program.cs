﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace FatimaSheremetyeva_DatabaseReview
{
    class Program
    {
        static void Main(string[] args)
        {
            string cs = @"server=localhost;userid=root;password=;database=sampleapidata; port=3306";

            MySqlConnection conn = null;

            do
            {
                Console.WriteLine("Please enter a city: ");
                string city = Console.ReadLine();
                try
                {
                    conn = new MySqlConnection(cs);
                    MySqlDataReader rdr = null;
                    conn.Open();

                    string stm = "select createdDate, description, temp, pressure, humidity, temp_min, temp_max, windSpeed from weather where city like '%" + city + "%' order by createdDate desc limit 1";
                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    rdr = cmd.ExecuteReader();
                    bool hitResult = false;
                    while (rdr.Read())
                    {
                        hitResult = true;
                        Console.WriteLine("Temperature is: " + rdr.GetString(2) + ", Pressure is:  " + rdr.GetString(3) + ", and Humidity is: " + rdr.GetString(4));
                    }
                    if (hitResult == false)
                    {
                        Console.WriteLine("No Data Available for the selected city.");
                    }

                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Error: {0}", ex.ToString());

                }
                finally
                {

                    if (conn != null)
                    {
                        conn.Close();
                    }

                }
            } while (true);
            Console.WriteLine("Done");
        }
    }
}
