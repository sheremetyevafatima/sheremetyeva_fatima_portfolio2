var data = 

{
  "fish": [
    {
      "type": "Salmon"
    },
    {
      "type": "Pike"
    },
    {
      "type": "Goldfish"
    },
    {
      "type": "Koi"
    },
    {
      "type": "Tuna"
    },
    {
      "type": "Albacore"
    },
    {
      "type": "Catfish"
    },
    {
      "type": "Cod"
    },
    {
      "type": "Shark"
    },
    {
      "type": "lutefisk"
    },
    {
      "type": "Carp"
    },
    {
      "type": "Suckermouth"
    },
    {
      "type": "Shark"
    },
    {
      "type": "Haddock"
    },
    {
      "type": "Swordfish"
    },
    {
      "type": "Blobfish"
    },
    {
      "type": "Bluefish"
    },
    {
      "type": "Tench"
    },
    {
      "type": "White"
    },
    {
      "type": "Mahi-Mahi"
    }
  ],
  "heron": [
    {
      "name": "Big Blue",
      "accuracy": 69
    },
    {
      "name": "Mighty Jack",
      "accuracy": 55
    },
    {
      "name": "The Big One",
      "accuracy": 99
    },
    {
      "name": "Tiny Boy",
      "accuracy": 9
    },
    {
      "name": "Fast One",
      "accuracy": 89
    },
    {
      "name": "The Bandit",
      "accuracy": 55
    },
    {
      "name": "Young Gun",
      "accuracy": 5
    },
    {
      "name": "The Old One",
      "accuracy": 19
    },
    {
      "name": "Warrior",
      "accuracy": 88
    },
    {
      "name": "Captain",
      "accuracy": 50
    },
    {
      "name": "Fast Bird",
      "accuracy": 79
    },
    {
      "name": "Night Bird",
      "accuracy": 53
    },
    {
      "name": "Da Baby",
      "accuracy": 1
    },
    {
      "name": "Albino",
      "accuracy": 58
    },
    {
      "name": "Bad Boy",
      "accuracy": 72
    },
    {
      "name": "The Hopeless One",
      "accuracy": 1
    },
    {
      "name": "The Best Bird",
      "accuracy": 99
    },
    {
      "name": "Baby B",
      "accuracy": 9
    },
    {
      "name": "Joe",
      "accuracy": 45
    },
    {
      "name": "Fatty",
      "accuracy": 15
    }
  ],
  "fisherman": [
    {
      "name": "Captain Morgan",
      "accuracy": 65
    },
    {
      "name": "Jack",
      "accuracy": 15
    },
    {
      "name": "B",
      "accuracy": 65
    },
    {
      "name": "Benedict",
      "accuracy": 65
    },
    {
      "name": "Jim",
      "accuracy": 15
    },
    {
      "name": "Frank",
      "accuracy": 56
    },
    {
      "name": "The Mad Captain",
      "accuracy": 95
    },
    {
      "name": "Captain Morgan",
      "accuracy": 65
    },
    {
      "name": "The Old Captain",
      "accuracy": 4
    },
    {
      "name": "Lady Fisherwoman",
      "accuracy": 95
    },
    {
      "name": "Bianca",
      "accuracy": 25
    },
    {
      "name": "Jose",
      "accuracy": 75
    },
    {
      "name": "Mike",
      "accuracy": 75
    },
    {
      "name": "Fatima",
      "accuracy": 55
    },
    {
      "name": "Stiff One",
      "accuracy": 85
    },
    {
      "name": "The Boss",
      "accuracy": 99
    },
    {
      "name": "Jason",
      "accuracy": 81
    },
    {
      "name": "Mocha"
    },
    {
      "name": "Senor",
      "accuracy": 45
    },
    {
      "name": "The Old Pole",
      "accuracy": 13
    }
  ]
};
