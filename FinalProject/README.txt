Predator Vs Fish

A JS console game. This is a game of heron/fishermen eating or not eating fish. The user's input
will influence if any fish will end up eaten by the predator. If the user
guess the system’s number no fish will be eaten or all fish will be eaten.



The research and assignments I have done this month helped me tremendously in completing my final project.
I used the following skills I learnt in this class:
-ECMA6JS
-OOP principles: Abstraction, Inheritance, Polymorphism (i.e super class, constructor, child classes extending super class)
-User Input (using prompt)
-User validation (i.e if the number is out of range or string, keep prompting)
-Loops (for, do-while)
-Ternary Statemements ( i.e var is_heron = Math.random() > 0.5 ? true : false;)
-JSON (i.e. types of fish and predatord are JSON objects)
-Array methods (i.e .pop, .push)
-Expressions (Math Object)
-Conditional Statements(i.e 
	if(todo == 'quit' )
		return;
	else
	{

		console.log("Battling!");)
-Functions (PredatorVsFish())
-Variables