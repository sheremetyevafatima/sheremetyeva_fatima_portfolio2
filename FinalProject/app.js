//Super class predator. Has 3 propertis: name, type and accuracy and getters of these properties

class Predator{

	//Constructor for super class
	constructor(name, type, accuracy) {
		this.name = name;
		this.type = type;
		this.accuracy = accuracy;
	}
	getAccuracy()
	{
		return this.accuracy;
	}
	getName()
	{
		return this.name;
	}
	getType()
	{
		return this.type;
	}
	//function to print the output
	toString()
	{
		return this.name + " is a " + this.type + " with an accuracy of " + this.accuracy;
	}
}

//child class heron extends predator class and inhertis same properties
class Heron extends Predator{
 constructor(name, accuracy)
 {
 	super(name, "heron", accuracy);
 }
}

//child class fisherman extends super class predator and inherits same properties
class Fisherman extends Predator{
	constructor(name, accuracy)
	{
		super(name, "fisherman", accuracy);
	}
}

//Fish class has 1 property and a function to return the value and a getter function.
class Fish {
	constructor(type)
	{
		this.type = type;
	}
	getType()
	{
		return this.type;
	}
}
// PredatorVsFish() function executes the logic of the game.
function PredatorVsFish()
{

	//Prompting the user to enter the number of fish 
	console.log("Welcome to Predator vs. Fish");
	do
	{
		var fish_count = prompt("To begin, enter how many fish you would like to have? (Between 1 and 20): ");
	}
	// if the number out of range or a string, keep prompting
	while(fish_count < 1 || fish_count > 20 || fish_count == "" || isNaN(fish_count));

    //prompt the user to enter the number of predators
	do
	{
		var predator_count = prompt("Enter how many predators are hunting for fish (Between 1 and 20): ");
	}
	// if the number out of range or a string, keep prompting
	while(predator_count < 1 || predator_count > 20 || predator_count == "" || isNaN(predator_count));

	var fishes = [];
	var predators = [];
    //creating arrays to set up the fisth based upon JSON data
	for(var i = 0; i < fish_count; i++)
	{
		fishes.push(new Fish(data.fish[i].type));
	}
    //creating arrays to set up predators based upon JSON data
	for(var i = 0; i < predator_count; i++)
	{

		//using random to make is_heron iether trur or false and execute conditional based on the result
		var is_heron = Math.random() > 0.5 ? true : false;
		if(is_heron)
			predators.push(new Heron(data.heron[i].name, data.heron[i].accuracy));
		else
			predators.push(new Fisherman(data.fisherman[i].name, data.fisherman[i].accuracy));
	}
	do
	{
		//Output types of fish to the user
console.log("Your current fish are: ")
	for(var i = 0; i < fish_count; i++)
	{
		console.log(fishes[i].getType());
	}

	//output current predators
console.log("Your current predators are: ")
	for(var i = 0; i < predator_count; i++)
	{
		console.log(predators[i]);
	}
//let user choose to battle or quit
	
	do
	{
		var todo = prompt("What do you want to do? quit / battle");
			todo = todo.toLowerCase();
			
	}
	while(todo !== "quit" && todo !== "battle");

	if(todo == 'quit' )
		return;
	else
	{

		console.log("Battling!");
		for(var i = 0; i < predator_count; i++)
		{


			//use random object to determine predators accuracy

			//output to the use the result of the battle
			var predatorGetsTheFish = Math.random() * 50 > predators[i].getAccuracy();

			if(predatorGetsTheFish)
			{
				var targetedFish = fishes.pop();
				console.log("Predator " + predators[i].getName() + " a " + predators[i].getType() + " caught " + targetedFish.getType());
			}

			fish_count = fishes.length;
			if(fish_count == 0)
			{
				console.log("All the fish are gone!!!");
				return;
			}
		}
	}


}
while(fish_count > 0 && predator_count > 0);
}
PredatorVsFish();

